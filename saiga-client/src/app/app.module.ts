import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { FeedComponent } from './components/feed/feed.component';
import { MessageboxComponent } from './components/messagebox/messagebox.component';
import { SidepanelComponent } from './components/sidepanel/sidepanel.component';
import { ChatComponent } from './components/chat/chat.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ErrormodalComponent } from './components/errormodal/errormodal.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { ChannelComponent } from './components/channel/channel.component';


@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    MessageboxComponent,
    SidepanelComponent,
    ChatComponent,
    WelcomeComponent,
    ErrormodalComponent,
    SearchbarComponent,
    ChannelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }