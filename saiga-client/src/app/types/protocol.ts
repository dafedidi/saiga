import { Profile } from "./profile";
import { Message } from "../types/message"

export enum ACTIONS{
    CONNECT = "CONNECT",
    MESSAGE = "MESSAGE",
    REQUIRE = "REQUIRE"
}

export enum REQUIRE_STATUS{
    REQUEST = "REQUEST",
    RESPONSE = "RESPONSE"
}

export enum REQUIRE_TYPE{
    PROFILE = "PROFILE",
    IMAGE = "IMAGE",
    HISTORY = "HISTORY",
}

export class ConnectRequest{
    public profile: Profile
    public action: ACTIONS = ACTIONS.CONNECT

    constructor(profile: Profile){
        this.profile = profile
    }

    public getJSON(){
        let data = {
            "name": this.profile.name,
            "public_id": this.profile.public_id,
            "image": this.profile.image,
            "action": this.action.toUpperCase()
        }
        return JSON.stringify(data)
    }
}

export class MessageRequest{
    public message: Message
    public action: ACTIONS = ACTIONS.MESSAGE

    constructor(message: Message){
        this.message = message
    }

    public getJSON(){
        let data ={
            "id": this.message.channelId,
            "text": this.message.text,
            "recipient": this.message.recipient,
            "sender": this.message.sender,
            "sender_name": this.message.senderName,
            "timestamp": this.message.timestamp.toISOString(),
            "action": this.action.toUpperCase()
        }

        return JSON.stringify(data)
    }
}

export class Require{
    public metadata: any
    public sender: string
    public recipient: string
    public action: ACTIONS = ACTIONS.REQUIRE
    public status: REQUIRE_STATUS
    public type: REQUIRE_TYPE

    constructor(metadata: any, type: REQUIRE_TYPE, status: REQUIRE_STATUS, sender: string, recipient: string){
        this.metadata = metadata
        this.status = status
        this.sender = sender
        this.recipient = recipient
        this.type = type
    }

    public getJSON(){
        let data= {
            "metadata": JSON.stringify(this.metadata),
            "status": this.status,
            "sender": this.sender,
            "recipient": this.recipient,
            "type": this.type
        }

        return JSON.stringify(data)
    }
}