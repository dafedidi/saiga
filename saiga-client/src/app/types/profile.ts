export class Profile {
    public name: string
    public public_id: string 
    public image: string | ArrayBuffer

    constructor(name: string, public_id: string, image: string){
        this.name = name
        this.public_id = public_id
        this.image = image
    }
}