export class Message {
    public text: string;
    public channelId: string;
    public recipient: Array<string>;
    public sender: string;
    public senderName: string
    public timestamp: Date;

    constructor(channelId: string, text : string, recipient: Array<string>, sender: string, senderName:string, timestamp: Date ) {
        this.channelId = channelId
        this.text = text
        this.recipient = recipient
        this.sender = sender
        this.senderName = senderName
        this.timestamp = timestamp
    }

    public setSender(sender: string, senderName: string){
        this.sender = sender
        this.senderName = senderName
    }

    public getJSON(){
        let data = {
            "text": this.text,
            "channelId": this.channelId,
            "sender": this.sender,
            "timestamp": this.timestamp.toISOString()
        }
        return JSON.stringify(data)
    }
}