export class Channel {
    public id: string;
    public participants: Array<string>;
    public owner: string;
    public name: string;

    constructor(id: string, participants: Array<string>, name: string, owner: string){
        this.id = id
        this.participants = participants
        this.name = name
        this.owner = owner
    }

    public getJSON(){
        let data = {
            "id": this.id,
            "participants": this.participants,
            "name": this.name,
            "owner": this.owner
        }
        return JSON.stringify(data)
    }
}
