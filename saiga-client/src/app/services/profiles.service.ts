import { Injectable } from '@angular/core';
import { Profile } from '../types/profile';
import {v4 as uuid} from 'uuid';
import { Observable, Subject} from 'rxjs';
import { ConnectionService } from './connection.service';
import { ConnectRequest } from '../types/protocol';
import { DatabaseService } from './database.service';

declare let forge: any;

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
  private profiles: Map<string, Profile>
  private ownProfile: Profile
  private db: IDBDatabase
  public isReadyEvent: Subject<boolean>

  constructor(private connection: ConnectionService, private database: DatabaseService) {
    this.profiles = new Map<string, Profile>()
    this.isReadyEvent = new Subject<boolean>()

    this.database.onIndexDBIsReady.subscribe((db)=>{
      this.db = db
      console.log("Received db as profile")
      this.isReadyEvent.next(true)
    })
  }

  sendProfileInfoToServer(){
    let connectRequest = new ConnectRequest(this.ownProfile)
    this.connection.send(connectRequest.getJSON())
  }

  createProfile(name, password, image){
    // Create two random uuid
    let private_id = uuid()
    let public_id = uuid()
    
    // Generate a hashed password with salt
    let md = forge.sha256.create()
    let hashedPassword = md.update(password+private_id)
    
    // Create the profile in the database
    this.db.transaction([this.database.profileDBName], "readwrite").objectStore(this.database.profileDBName).add({"name": name, "password": hashedPassword.digest().toHex(), "public_id": public_id, "image":image, "private_id": private_id})
    this.ownProfile = new Profile(name, public_id, image)
 
    // Keep this account connected unless he logs out
    localStorage.setItem('last_connected_public_id', this.ownProfile.public_id)

    this.sendProfileInfoToServer()
  }

  addPublicProfileToDatabase(profile: Profile){
    this.db.transaction([this.database.publicProfilesDBName], "readwrite").objectStore(this.database.publicProfilesDBName).put({"name": profile.name, "image": profile.image, "public_id": profile.public_id})
  }

  login(username, password): Observable<Profile>{
    let observable = new Observable<Profile>((observer) => {
      let request = this.db.transaction([this.database.profileDBName], "readonly").objectStore(this.database.profileDBName).index("name").get(username)
      
      request.onerror = (error: any) =>{
        observer.error(error)
      }

      request.onsuccess = (event: any) =>{
        if(event.target.result === undefined){
          observer.next(undefined)
          return
        }

        let md = forge.sha256.create()
        let hashedPassword = md.update(password+event.target.result.private_id)
        if(hashedPassword.digest().toHex() !== event.target.result.password){
          observer.next(undefined)
          observer.complete()
          return
        }

        this.ownProfile = new Profile(event.target.result.name, event.target.result.public_id, event.target.result.image)
        // Keep this account connected unless he logs out
        localStorage.setItem('last_connected_public_id', this.ownProfile.public_id)
        this.sendProfileInfoToServer()

        observer.next(this.ownProfile)
        observer.complete()
      }
    });

    return observable
  }

  fastlogin(public_id: string): Observable<Profile>{
    let observable = new Observable<Profile>((observer) => {
      this.fetchOwnProfile(public_id).subscribe((profile)=>{
        if(profile === undefined){
          observer.next(undefined)
          observer.complete()
          return
        }
        this.ownProfile = profile
        this.sendProfileInfoToServer()
        observer.next(profile)
        observer.complete()
      })
    })

    return observable
  }

  logout(){
    // No more auto connect
    localStorage.setItem('last_connected_public_id', '')
  }

  validateProfileExist(username): Observable<boolean>{
    let observable = new Observable<boolean>((observer) => {
      let request = this.db.transaction([this.database.profileDBName], "readonly").objectStore(this.database.profileDBName).index("name").get(username)
      
      request.onerror = (error: any) =>{
        observer.error(error)
      }

      request.onsuccess = (event: any) =>{
        observer.next(event.target.result !== undefined)
        observer.complete()
      }
    });

    return observable
  }

  fetchOwnProfile(public_id): Observable<Profile>{
    let observable = this.fetchProfile(public_id, this.database.profileDBName)
    return observable
  }

  fetchPublicProfile(public_id): Observable<Profile>{
    let observable = this.fetchProfile(public_id, this.database.publicProfilesDBName)
    return observable
  }

  fetchProfile(public_id: string, objectStorage: string): Observable<Profile>{
    let observable = new Observable<Profile>((observer) => {
      let request = this.db.transaction([objectStorage], "readonly").objectStore(objectStorage).get(public_id)

      request.onerror = (error: any) =>{
        observer.error(error)
      }

      request.onsuccess = (event: any) =>{
        if(event.target.result === undefined){
          observer.next(undefined)
          observer.complete()
          return 
        }

        let profile = new Profile(event.target.result.name, event.target.result.public_id, event.target.result.image)
        this.profiles.set(profile.public_id, profile)
        observer.next(profile)
        observer.complete()
      }
    });

    return observable
  }

  fetchProfileFromServer(public_id) : Observable<Profile>{
    let observable = new Observable<Profile>((observer)=>{
      let url = `http://${window.location.hostname}:3000/ids/${public_id}`

      fetch(url).then(r => r.json(), e => console.error(e)).then( data => {
        let profile = new Profile(data["name"], data["public_id"], data["image"])
        this.addOrUpdateProfile(profile)
        observer.next(profile)
        observer.complete()
      })
    })
    
    return observable
  }

  // TODO dbarampanze : should search locally first before doing a query
  // but for simplicity sake for the moment we search directly on the server
  searchProfiles(name): Observable<Array<Profile>>{
    let observable = new Observable<Array<Profile>>((observer)=>{
      let url = `http://${window.location.hostname}:3000/users?name=${name}`

      fetch(url).then(r => r.json(), e => console.error(e)).then( data => {
        let profiles = new Array<Profile>()
        data.forEach(element => {
          let obj = JSON.parse(element)
          let profile = new Profile(obj["name"], obj["public_id"], obj["image"])
          profiles.push(profile)
        });

        observer.next(profiles)
        observer.complete()
      })
    })

    return observable
  }

  public getOwnProfile(){
    return this.ownProfile
  }

  public getProfile(public_id:string){
    return this.profiles.get(public_id) 
  }

  public addOrUpdateProfile(profile: Profile){
    this.addPublicProfileToDatabase(profile)
    this.profiles[profile.public_id] = profile
  }
}
