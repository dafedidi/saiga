import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ACTIONS } from '../types/protocol';
import { Message } from '../types/message';

@Injectable({
providedIn: 'root'
})

export class ConnectionService {
    socket: any;
    queue : Array<any>;
    reconnectionTimer : number = 1000;

    public onReceiveMessage: Subject<Message>
    public onReceiveRequest: Subject<Request>
    public onConnect: Subject<boolean>

    constructor(){
        this.queue = new Array<any>()
        this.onReceiveMessage = new Subject<Message>()
        this.onConnect = new Subject<boolean>()
        this.bindSocket()
    }

    isReady(){
        return this.socket.readyState === WebSocket.OPEN
    }

    reconnect(){
        if(this.socket.readyState !== WebSocket.OPEN){
            setTimeout(()=>{
                this.reconnect()
            }, this.reconnectionTimer)

            this.bindSocket()
            return
        }

        console.log("Reconnection sucessful")
    }

    bindSocket() {
        this.socket = new WebSocket(`ws://${window.location.hostname}:3000/connect`)
        this.socket.onopen = this.open.bind(this)
        this.socket.onmessage = this.receive.bind(this)
        this.socket.onerror = this.error.bind(this)
    }

    open(){
        console.debug("Connection is open with server")
        this.socket.onclose = this.close.bind(this)
        this.onConnect.next(true)
    }

    public TryFlushNotSentQueue(){
        while(this.queue.length > 0){
            this.socket.send(this.queue.pop())
        }
    }

    receive(data){
        console.debug("new message received")
        
        let webSocketMessage = JSON.parse(data.data)
        switch(webSocketMessage["action"]){
          case ACTIONS.MESSAGE:{
            let message = new Message(webSocketMessage["id"], webSocketMessage["text"], webSocketMessage["recipient"], webSocketMessage["sender"], webSocketMessage["sender_name"], new Date(webSocketMessage["timestamp"]))
            this.onReceiveMessage.next(message)
          }
          case ACTIONS.REQUIRE: {
            // TODO @dbarampanze : handle other cases
          }
        }
    }

    send(data){
        if(this.socket.readyState !== WebSocket.OPEN){
            this.queue.push(data)
            console.log("Queued message")
            return
        }

        this.socket.send(data)
    }

    error(event){
        console.error(event)
        this.onConnect.next(false)
    }

    close(event){
        console.debug(event)
        console.info("Lost connection. Reconnecting...")
        //this.reconnect()
    }
}
