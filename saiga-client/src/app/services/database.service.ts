import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  readonly channelDBName: string = "channels"
  readonly historyDBName: string = "history"
  readonly profileDBName: string = "profiles"
  readonly publicProfilesDBName: string = "publicProfiles"
  public onIndexDBIsReady : Subject<IDBDatabase>
  private db: IDBDatabase

  constructor() { 
    if(!window.indexedDB){
      console.error("Your browser does not support indexDB, please upgrade to something good.")
      return
    }

    this.onIndexDBIsReady = new Subject<IDBDatabase>();

    let indexRequestDB: IDBOpenDBRequest = window.indexedDB.open("SaigaDatabase", 1)
    indexRequestDB.onerror = this.error.bind(this)
    indexRequestDB.onsuccess  = this.success.bind(this)
    indexRequestDB.onupgradeneeded = this.onupgradeneeded.bind(this)
  }

  public isReady(){
    return this.db !== undefined
  }

  private error(event){
    console.error("Database error: " + event)
    this.onIndexDBIsReady.next(undefined)
  }

  private success(event){
    this.db = event.target.result
    this.onIndexDBIsReady.next(this.db)
  }

  private onupgradeneeded(event){
    this.db = event.target.result

    this.setupProfiles()
    this.setupChat()

    this.onIndexDBIsReady.next(this.db)
  }

  private setupProfiles(){
    // Store profiles on the user machine (registered)
    let profileStore = this.db.createObjectStore("profiles", {keyPath: "public_id", autoIncrement: false})
    // Store public profiles of other users
    let publicProfileStore = this.db.createObjectStore("publicProfiles", {keyPath: "public_id", autoIncrement: false})

    // Allow search by public_id
    profileStore.createIndex("name","name", {unique: false})
    publicProfileStore.createIndex("name","name", {unique: false})

    profileStore.transaction.oncomplete = () =>{
        //Nothing for the moment, but we will be able to do the transaction of items here
    }

    publicProfileStore.transaction.oncomplete = () =>{
      //Nothing for the moment, but we will be able to do the transaction of items here
    }
  }

  private setupChat(){
    // Store all the conversation, called channels
    let channelStore = this.db.createObjectStore(this.channelDBName, {keyPath: "id", autoIncrement: false})
    // Store the history of conversations
    let historyStore = this.db.createObjectStore(this.historyDBName, {keyPath: "timestamp", autoIncrement: false})

    // Allow search by name for channels
    channelStore.createIndex("name","name", {unique: false})

    channelStore.transaction.oncomplete = () =>{
        //Nothing for the moment, but we will be able to do the transaction of items here
    }

    historyStore.transaction.oncomplete = () =>{
      //Nothing for the moment, but we will be able to do the transaction of items here
    }
  }
}
