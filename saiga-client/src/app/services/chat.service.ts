import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { ProfilesService } from './profiles.service';
import { ConnectRequest, MessageRequest, ACTIONS, Require, REQUIRE_TYPE, REQUIRE_STATUS } from '../types/protocol';
import { Message} from '../types/message';
import { Subject, Observable, Subscribable } from 'rxjs';
import { Channel } from '../types/channel';
import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  public newMessageEvent : Subject<Message>
  public onIndexDBIsReady : Subject<boolean>
  public onChangeChannel: Subject<Channel>
  public onCreateChannel: Subject<Channel>
  private currentChannel: Channel
  public isReadyEvent: Subject<boolean>
  private db: IDBDatabase

  constructor(private connection: ConnectionService, private profiles : ProfilesService, private database: DatabaseService) {
    this.newMessageEvent = new Subject<Message>()
    this.connection.onReceiveMessage.subscribe((message) => this.receiveData(message))
    this.onChangeChannel = new Subject<Channel>()
    this.onCreateChannel = new Subject<Channel>()
    this.isReadyEvent = new Subject<boolean>()

    this.database.onIndexDBIsReady.subscribe((db)=>{
      this.db = db
      console.log("Received db as chat")
      this.isReadyEvent.next(true)
    })
  }

  public SendMessageTo(text: string){
    let message = new Message(this.getCurrentChannel().id, text, this.getCurrentChannel().participants, this.profiles.getOwnProfile().public_id, this.profiles.getOwnProfile().name, new Date())
    let messageRequest = new MessageRequest(message)
    this.connection.send(messageRequest.getJSON())

    if(this.getCurrentChannel().id !== "general"){
      this.addMessageToHistory(message)
    }
    
    return message
  }

  private receiveData(message: Message){
    this.addMessageToHistory(message)
    this.newMessageEvent.next(message)
  }

  public createChannel(channel: Channel){
    this.db.transaction([this.database.channelDBName], "readwrite").objectStore(this.database.channelDBName).add(channel)
    this.onCreateChannel.next(channel)
  }

  public getChannelByName(name: string): Observable<Channel>{
    let observable = new Observable<Channel>((observer) => {
      let request = this.db.transaction([this.database.channelDBName], "readonly").objectStore(this.database.channelDBName).index("name").get(name)

      request.onerror = (error: any) =>{
        observer.error(error)
      }

      request.onsuccess = (event: any) =>{
        if(!event.target.result){
          observer.next(undefined)
          observer.complete()
          return
        }

        if(event.target.result.owner !== this.profiles.getOwnProfile().public_id){
          observer.next(undefined)
          observer.complete()
          return
        }

        let channel = new Channel(event.target.result.id, event.target.result.participants, event.target.result.name, event.target.result.owner)
        observer.next(channel)
        observer.complete()
      }
    })

    return observable
  }

  public getChannels(limit: number) : Observable<Array<Channel>>{
    let observable = new Observable<Array<Channel>>((observer) => {
      let request = this.db.transaction([this.database.channelDBName], "readonly").objectStore(this.database.channelDBName).getAll(null, limit)
      
      request.onerror = (error: any) =>{
        observer.error(error)
      }

      request.onsuccess = (event: any) =>{
        let values = new Array<Channel>()
        event.target.result.forEach((element)=>{
          if(element.owner !== this.profiles.getOwnProfile().public_id){
            return
          }

          values.push(new Channel(element.id, element.participants, element.name, element.owner))
        })

        observer.next(values)
        observer.complete()
      }
    })

    return observable
  }

  public addMessageToHistory(message: Message){
    this.db.transaction([this.database.historyDBName], 'readwrite').objectStore(this.database.historyDBName).add(message)
  }

  public getMessagesFromHistory(from: Date, to: Date, channelId: string): Observable<Array<Message>>{
    let observable = new Observable<Array<Message>>((observer)=>{
      let boundKeyRange = IDBKeyRange.bound(from, to, true, true)
      let request = this.db.transaction([this.database.historyDBName], 'readonly').objectStore(this.database.historyDBName).getAll(boundKeyRange)

      request.onerror = (error: any) =>{
        observer.error(error)
      }

      request.onsuccess = (event: any) =>{
        let values = new Array<Message>()

        if(!event.target.result){
          observer.next(values)
          observer.complete()
          return
        }

        event.target.result.forEach(element => {
          if(channelId !== element.channelId){
            return
          }
          
          values.push(new Message(element.channelId, element.text, element.recipient, element.sender, element.senderName, element.timestamp))
        });

        observer.next(values)
        observer.complete()
      }
    })

    return observable
  }

  public setChannel(channel: Channel){
    this.currentChannel = channel
    this.onChangeChannel.next(this.currentChannel)
  }

  public getCurrentChannel(){
    return this.currentChannel
  }
}
