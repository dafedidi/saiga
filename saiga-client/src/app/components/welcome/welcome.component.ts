import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfilesService } from 'src/app/services/profiles.service';
import { Profile } from 'src/app/types/profile';
import { DatabaseService } from 'src/app/services/database.service';
import { ConnectionService } from 'src/app/services/connection.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  // Application is ready to start
  isDbReady: boolean
  isConnectionReady: boolean
  errorDBMessage: string
  isInError: boolean

  // Login information
  profile: Profile
  password: string
  hasLoginError: boolean
  loginErrorMessage: string

  // Register information
  registerProfile: Profile
  registerPassword: string
  confirmedPassword: string
  hasRegisterError: boolean
  registerErrorMessage: string

  constructor(private profiles : ProfilesService, private router: Router, private database: DatabaseService, private connection : ConnectionService, private chat: ChatService) {
    this.profile = new Profile('', '', '')
    this.registerProfile = new Profile('', '', 'assets/avatar.png')
    this.hasLoginError = false
    this.hasRegisterError = false
    this.isInError = false
    this.isDbReady = false
    this.isConnectionReady = false

    this.connection.onConnect.subscribe((isReady)=>{
      this.isConnectionReady = isReady
      if(!this.isConnectionReady){
        this.isInError = true
        this.errorDBMessage = "Failed to connect to server. Please retry later"
        return
      }

      this.CheckIfLoggedIn()
    })

    this.database.onIndexDBIsReady.subscribe((db)=>{
      this.isDbReady = db !== undefined

      if(!this.isDbReady){
        this.isInError = true
        this.errorDBMessage = "Index DB failed, please upgrade to a browser that support index db."
        return
      }

      this.CheckIfLoggedIn()
    })

    this.isDbReady = this.database.isReady() 
    this.isConnectionReady = this.connection.isReady()
  }

  ngOnInit(): void {
    this.CheckIfLoggedIn()
  }

  CheckIfLoggedIn(){
    if(!this.isDbReady || !this.isConnectionReady){
      return
    }

    let public_id = localStorage.getItem('last_connected_public_id')
  
    if(public_id === ''){
      return
    }

    this.profiles.fastlogin(public_id).subscribe((profile)=>{
      if(profile !== undefined){
        this.router.navigate(['chat'])
      }
    })
  }

  processFile(imageInput){
    if(imageInput.files.length <= 0)
      return

    const file: File = imageInput.files[0];
    let reader = new FileReader()

    reader.readAsDataURL(file)
    reader.onload = (e) =>{
      this.registerProfile.image = e.target.result
    }
  }

  login(){
    this.profiles.login(this.profile.name, this.password).subscribe(
      (profile) =>{
        if(profile !== undefined){
          this.router.navigate(['chat'])
        }
        else{
          this.password = ""
          this.profile.name = ""
          this.hasLoginError = true
          this.loginErrorMessage = "Wrong username or password."
        }
      }
    )
  }

  register(){
    if(this.confirmedPassword !== this.registerPassword){
      this.registerPassword = ""
      this.confirmedPassword = ""
      this.hasRegisterError = true
      this.registerErrorMessage = "The confirmed password and password fields are not equal." 
      return
    }

    this.profiles.validateProfileExist(this.registerProfile.name).subscribe(
    (exist) =>{
      if(!exist){
        this.profiles.createProfile(this.registerProfile.name, this.registerPassword, this.registerProfile.image)
        this.router.navigate(['chat'])
      }
      else{
        this.registerPassword = ""
        this.confirmedPassword = ""
        this.hasRegisterError = true
        this.registerErrorMessage = "An account with this name already exists." 
      }
    })
  }
}
