import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Message } from 'src/app/types/message';
import { ChatService } from 'src/app/services/chat.service';
import { Channel } from 'src/app/types/channel';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit, AfterViewChecked {
  public messageToSend: string = ''
  public messages: Array<Message>
  @ViewChild('feedContent')
  feedElement: ElementRef

  constructor(private chat: ChatService) { 
    this.messages = new Array<Message>()
    this.chat.onChangeChannel.subscribe((channelId)=>{
      this.onChangeChannel(channelId)
    })
    this.chat.newMessageEvent.subscribe((message)=>this.newMessageReceived(message))
  }

  ngOnInit(): void {   
  }

  ngAfterViewChecked() {        
    this.scrollToEnd()        
} 

  onChangeChannel(channel: Channel){
    this.messages = new Array<Message>()
    let currentDate = new Date()
    let previousDate = new Date()
    previousDate.setDate(currentDate.getDate() - 1)
    this.chat.getMessagesFromHistory(previousDate, currentDate, channel.id).subscribe((messages)=>{
      messages.forEach(message => this.newMessageReceived(message))
    })
  }

  newMessageReceived(message: Message){
    if(message.channelId !== this.chat.getCurrentChannel().id)
      return
    
    this.messages.push(message)
  }

  sendMessage(){
    if(this.messageToSend === ""){
      return
    }

    let message = this.chat.SendMessageTo(this.messageToSend)

    if(this.chat.getCurrentChannel().id !== "general"){
      this.messages.push(message)
    }
    // Empty the message box as a feedback for sending message
    this.messageToSend = ''
  }

  scrollToEnd(){
    this.feedElement.nativeElement.scrollTop = this.feedElement.nativeElement.scrollHeight
  }
}
