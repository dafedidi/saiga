import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/types/profile';
import { ProfilesService } from 'src/app/services/profiles.service';
import { ChatService } from 'src/app/services/chat.service';
import { Channel } from 'src/app/types/channel';
import {v4 as uuid} from 'uuid';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {
  public profiles: Array<Profile>
  public searchInput : string

  constructor(private profileService : ProfilesService, private chat: ChatService) { 
    this.profiles = new Array<Profile>()
  }

  ngOnInit(): void {
  }

  onSearch(event: any){
    if(event.target.value.length <= 2){
      this.profiles = new Array<Profile>()
      return
    }

    this.profileService.searchProfiles(event.target.value).subscribe((profiles)=>{
      this.profiles = profiles
    })
  }

  onSelectProfile(event: any){
    let public_id = ""
    if(event.target.nodeName !== "DIV"){
      public_id = event.target.parentElement.id
    }
    else{
      public_id = event.target.id
    }

    let profile = this.profiles.find(element => element.public_id === public_id)

    this.getOrCreateChannel(profile)

    this.profiles = new Array<Profile>()
    this.searchInput = ""
  }

  getOrCreateChannel(profile: Profile){
    this.chat.getChannelByName(profile.name).subscribe((channel)=>{
      if(channel === undefined){
        let newchannel = new Channel(uuid(), [profile.public_id], profile.name, this.profileService.getOwnProfile().public_id)
        this.chat.createChannel(newchannel)
        this.chat.setChannel(newchannel)
      }
      else{
        this.chat.setChannel(channel)
      }
    })
  }

}
