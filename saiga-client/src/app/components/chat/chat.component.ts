import { Component, OnInit } from '@angular/core';
import { ProfilesService } from 'src/app/services/profiles.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  constructor(private profiles: ProfilesService, private router : Router) {
    if(this.profiles.getOwnProfile() === undefined) {
      this.router.navigate(['login'])
    }
   }

  ngOnInit(): void {
  }

}
