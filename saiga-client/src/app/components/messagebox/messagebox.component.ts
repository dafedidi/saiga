import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../types/message';
import { ProfilesService } from 'src/app/services/profiles.service';

@Component({
  selector: 'app-messagebox',
  templateUrl: './messagebox.component.html',
  styleUrls: ['./messagebox.component.css']
})
export class MessageboxComponent implements OnInit {
  @Input()
  messageInternal: Message
  isOwnMessage: boolean
  image: string | ArrayBuffer

  constructor(private profiles: ProfilesService) { 
    this.isOwnMessage = false
  }

  ngOnInit(): void {
    this.isOwnMessage = this.profiles.getOwnProfile().public_id === this.messageInternal.sender
    
    if(this.isOwnMessage){
      this.image = this.profiles.getOwnProfile().image
    }
    else{
      let profile = this.profiles.getProfile(this.messageInternal.sender)
      if(profile === undefined) {
        this.fetchPublicProfile()
        return
      }

      this.image = profile.image
    }
  }

  fetchPublicProfile(){
    this.profiles.fetchPublicProfile(this.messageInternal.sender).subscribe((profile)=>{
      if(profile === undefined){
        this.profiles.fetchProfileFromServer(this.messageInternal.sender).subscribe((profile) =>{
          if(profile === undefined)
            return
           
          this.image = profile.image
        })
        return
      }

      this.image = profile.image
    })
    
  }
}
