import { Component, OnInit } from '@angular/core';
import { Channel } from 'src/app/types/channel';
import { ChatService } from 'src/app/services/chat.service';
import { element } from 'protractor';
import { ProfilesService } from 'src/app/services/profiles.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidepanel',
  templateUrl: './sidepanel.component.html',
  styleUrls: ['./sidepanel.component.css']
})
export class SidepanelComponent implements OnInit {
  public channels : Array<Channel>

  constructor(public chat: ChatService, public profile: ProfilesService, public router: Router) {
    this.channels = new Array<Channel>()
    let publicChannel = new Channel("general", ["*"], "general", this.profile.getOwnProfile().public_id)
    this.channels.push(publicChannel)
    this.chat.setChannel(publicChannel)
  }

  ngOnInit(): void {
    this.chat.getChannels(10).subscribe((historychannels)=>{
      this.channels = this.channels.concat(historychannels)
    })

    this.chat.newMessageEvent.subscribe((message)=>{
      let channelIdx = this.channels.findIndex(element => element.id === message.channelId)
      if(channelIdx === -1){
        let channel = new Channel(message.channelId, [message.sender], message.senderName, this.profile.getOwnProfile().public_id)
        this.chat.createChannel(channel)
        return
      }

      let temp = this.channels[0]
      this.channels[0] = this.channels[channelIdx]
      this.channels[channelIdx] = temp
    })

    this.chat.onCreateChannel.subscribe((channel)=>{
      this.channels.push(channel)
    })
  }

  onClickChannel(event:any){
    let channel = this.channels.find(element => element.id === event.target.id)

    if(channel === undefined){
      return
    }

    this.chat.setChannel(channel)
  }

  logout(){
    this.profile.logout()
    this.router.navigate(['login'])
  }
}
