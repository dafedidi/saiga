import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-errormodal',
  templateUrl: './errormodal.component.html',
  styleUrls: ['./errormodal.component.css']
})
export class ErrormodalComponent implements OnInit {
  @Input()
  errorMessage: string
  
  constructor() { }

  ngOnInit(): void {
  }
}
