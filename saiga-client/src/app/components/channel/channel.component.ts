import { Component, OnInit, Input } from '@angular/core';
import { ProfilesService } from 'src/app/services/profiles.service';
import { Profile } from 'src/app/types/profile';
import { Channel } from 'src/app/types/channel';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {
  @Input()
  public channel: Channel
  public profile: Profile //TODO dbarampanze Support one to one at the moment : needs to become Array<Profiles>

  constructor(private profiles: ProfilesService, private chat: ChatService) { 
  }

  ngOnInit(): void {
    if(this.channel.id === "general"){
      this.profile = new Profile("General", "*", "assets/Saiga.jpg")
      return
    }


    // TODO dbarampanze - support only one profile at the moment
    this.profiles.fetchPublicProfile(this.channel.participants[0]).subscribe((profile)=>{
      if(profile === undefined){
        this.profiles.fetchProfileFromServer(this.channel.participants[0]).subscribe((profile)=>{
          this.profile = profile
        })
        return
      }
      this.profile = profile
    })
  }
}
