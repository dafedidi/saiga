var ws = require('ws')

var publicIdToUser = new Map()
var nameToUser = new Map()

//TODO dbarampanze: To implement at some point
var offlineMessage = new Map()

class User{
    constructor(name, public_id, image, socket){
        this.public_id = public_id
        this.name = name
        this.socket = socket
        this.image = image
    }

    getJson(){
        let data ={
            "name": this.name,
            "public_id": this.public_id,
            "image": this.image,
        }

        return JSON.stringify(data)
    }
}

function initsocket(socket){
    socket.on('close', (code, reason) => close(socket, code, reason))
    socket.on('message', (data) => message(socket, data))
}

function findUserByName(username){
    return nameToUser.get(username)
}

function findUserByPublicId(public_id){
    return publicIdToUser.get(public_id)
}

function close(socket, code, reason){
    console.debug("Connection closed with websocket. Code ", code, " reason - ", reason)

    removeClient(publicIdToUser, socket)
    removeClient(nameToUser, socket)
}

function removeClient(mapping, socket){
    let keyOfClient = ""

    for(let [key, value] of mapping.entries()){
        if(value.socket == socket){
            keyOfClient = key
            break
        }
    }

    if(keyOfClient !== ""){
        mapping.delete(keyOfClient)
        console.debug(`Client ${keyOfClient} left.`)
    }
}

function message(socket, data){
    console.debug("New message received ")
    
    let info = JSON.parse(data)

    if(info["action"] === "CONNECT"){
        let user = new User(info["name"], info["public_id"], info["image"], socket)
        
        publicIdToUser.set(info["public_id"], user)
        nameToUser.set(info["name"], user)

        console.debug(`Added ${info["public_id"]} as ${info["name"]} to the list of available users`)

        let messages = offlineMessage.get(user.public_id)

        if(messages === undefined){
            return
        }

        messages.forEach(element => {
            user.socket.send(element)
        });

        offlineMessage.delete(info["public_id"])

    }
    else{
        if(info["recipient"][0] === "*"){
            publicIdToUser.forEach((value, key, map) =>{
                value.socket.send(data)
            })
            return
        }

        info["recipient"].forEach(element => {
            let user = publicIdToUser.get(element)
            
            // Store offline message for a delivery at a later time
            if(user === undefined) {
                let messages = offlineMessage.get(element)
                if(messages === undefined){
                    offlineMessage.set(element, [data])
                    console.debug("created offline message")
                }
                else{
                    messages.push(data)
                    console.debug("added offline message")
                }
                return
            }

            // user is connected send it now
            user.socket.send(data)
        });
    }
}

function searchUserByName(name){
    let matches = []
    for(let [key, value] of nameToUser.entries()){
        if(key.toLowerCase().startsWith(name.toLowerCase())){
            matches.push(value.getJson())
        }
    }

    return matches
}

module.exports = {initsocket, User, findUserByName, findUserByPublicId, searchUserByName}