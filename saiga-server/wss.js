var websocket = require('ws')
var relay = require('./relay.js')

var websocketServer;

function initWebSocketServer(applicationServer) {
    websocketServer = new websocket.Server({server: applicationServer, path: '/connect'})

    websocketServer.on('connection', (socket, req) =>{
        console.debug("Websocket server received new connection")

        relay.initsocket(socket)
    })
    
    websocketServer.on('error', (err)=>{
        console.error(err)
    })
    
    websocketServer.on('close', (socket)=>{
        console.log("Websocket server is closed")
    })
    
    websocketServer.on('listening', ()=>{
        console.debug("Websocket server is listenning")
    })
}

module.exports = { initWebSocketServer }