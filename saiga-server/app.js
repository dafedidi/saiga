var http = require('http')
var express = require('express');
var app = express()
var wss = require('./wss.js')
var relay = require('./relay.js')
var cors = require('cors')

const hostname = '0.0.0.0';
const port = 3000;

app.use(cors())

var server = http.createServer(app)

app.get('/version', (req, res) =>{
  res.send("0.1.0")
})

/* Get a specific user by name if connected */
app.get('/users/:username', (req, res)=>{
  let username = req.params.username
  let user = relay.findUserByName(username)

  if(user === undefined){
    res.send({})
    res.status(404)
    return
  }

  res.send(user.getJson())
  res.status(200)
})

/* Get a specific user by public_id if connected */
app.get('/ids/:id', (req, res)=>{
  let id = req.params.id
  let user = relay.findUserByPublicId(id)
  
  if(user === undefined){
    res.send({})
    res.status(404)
    return
  }
  
  res.send(user.getJson())
  res.status(200)
})

/* To search users by name */
app.get('/users', (req, res)=>{
  let nameSearched = relay.searchUserByName(req.query.name)
  res.send(JSON.stringify(nameSearched))
  res.status(200)
})

/* To search users by public_id */
app.get('/ids', (req, res)=>{
  
})

wss.initWebSocketServer(server)


server.listen(port, hostname, ()=> console.log(`Server running at http://${hostname}:${port}/`))
